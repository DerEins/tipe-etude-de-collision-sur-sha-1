#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import outils
import random as rd

T0=list(range(256))

def KSA(K,n=256):
    """
    KSA est la première partie de l'algorithme de chiffrement par flux RC4. Son but est de créér une table de valeurs comprises entre 0 et 255 qu'il mélange pseudo-aléatoirement pour l'algorithme PRGA.
    Cette version permet de n'exécuter qu'un certain nombre d'itération de KSA et renvoie, en plus de la table, les valeurs des rangs x et y au moment de l'arrêt de l'algorithme.

    Synthaxe : KSA(Key,n=0)

    Paramètres :
        - Key : Clé que l'on souhaite utiliser pour exécuter RC4 (bytes).
        - n : Optionnel. Nombre d'itération de KSA souhaitée (int).

    Retour : Liste d'entiers, valeurs des rangs à l'arrêt de l'algorithme.
    """
    T=T0.copy()
    y=0
    for x in range(0,n):
        y=(y+T[x]+K[x%len(K)])%256
        T[x],T[y]=T[y],T[x]
    return T,x,y

def PRGA(T,x=0,y=0):
    """
    PRGA est la seconde partie de l'algorithme de chiffrement par flux RC4. Son but est de générer le flux d'octet nécessaire au chiffrement à partir de la table générée par KSA.
    Cette version permet de choisir les rangs x et y  pour lesquel on veut exécuter PRGA, et notamment ceux données par KSA.

    Synthaxe : PRGA(T,x=0,y=0)

    Paramètres:
        - T : Table de valeurs générée par KSA (liste de int).
        - x : Optionnel (x=0 par défaut). Rang choisi pour exécuter PRGA (int).
        - y : Optionnel (y=0 par défaut). Rang choisi pour exécuter PRGA (int).
        
    Retour : octet généré par RC4 (bytes), valeurs des rangs à l'arrêt de l'algorithme (int).
    """
    x=(x+1)%256
    y=(y+T[x])%256
    T[x],T[y]=T[y],T[x]
    z=(T[x]+T[y])%256
    return T[z].to_bytes(1, byteorder='big'),x,y

def chiffrer(Key,Message):
    """
    Algorithme complet de RC4, basés sur KSA et PRGA. Permet à la fois de chiffrer et de déchiffrer, RC4 étant symétrique.

    Synthaxe : chiffrer(Key,Message)

    Paramètres:
        - Key : Clé de chiffrement utilisé pour RC4 (byte). Correspond à IV||CléWEP pour WEP.
        - Message : Message à chiffrer (bytes).

    Retour : Message chiffré par RC4 (bytes).
    """
    P=KSA(Key)[0]
    O=PRGA(P)
    octets=O[0]
    for i in range(len(Message)-1):
        O=PRGA(P,O[1],O[2])
        octets+=O[0]
    return outils.byte_xor(octets,Message)