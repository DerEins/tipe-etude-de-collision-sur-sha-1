#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Created on Fri May 15 13:11:18 2020

@author: mathieu
"""

"""
def oct_to_bin(octet):
    retour=[]
    for i in range(8):
        if octet - 2**1 >= 0:
            retour.append("1")
            octet = octet - 2**i
        else:
            retour.append("0")
    return "".join(retour[::-1])
"""

def sha1(data):
    #print(oct_to_bin(data))
    formatInitial= ""
    h0 = 0x67452301
    h1 = 0xEFCDAB89
    h2 = 0x98BADCFE
    h3 = 0x10325476
    h4 = 0xC3D2E1F0
    for n in range(len(data)):
        formatInitial+='{0:08b}'.format(ord(data[n])) #Converion en binaire par octet
    #print(formatInitial)
    formatInitial+='1' #Ajout d'un bit 1 à la fin de la chaîne de caractère.
    format512 = formatInitial
    #On complète avec des zéros à droite jusqu'à len(condense) = 448 [512]
    while len(format512)%512 != 448:
        format512+="0"
    #On ajoute la longueur du message au format 64 bits à droite.
    format512+='{0:064b}'.format(len(formatInitial)-1) #Comm    
    
    def portion(l, n): #Fonction renvoyant une portion donné d'une chaîne de caractère/
        return [l[i:i+n] for i in range(0, len(l), n)]

    def rotl(n, b):
        """Fontion rotation de b bits à gauche"""
        return ((n << b) | (n >> (32 - b))  ) & 0xffffffff

    for c in portion(format512, 512): 
        words = portion(c, 32) #Découpage en mots de 32 bits
        w = [0]*80 #Préparation de la reconversion en entiers
        for n in range(0, 16):
            w[n] = int(words[n], 2) 

        for i in range(16, 80):
            w[i] = rotl((w[i-3] ^ w[i-8] ^ w[i-14] ^ w[i-16]), 1) 

        a,b,c,d,e = h0,h1,h2,h3,h4

        #Boucle principale
        for i in range(0, 80):
            if 0 <= i <= 19:
                f = (b & c) | ((~b) & d)
                k = 0x5A827999
            elif 20 <= i <= 39:
                f = b ^ c ^ d
                k = 0x6ED9EBA1
            elif 40 <= i <= 59:
                f = (b & c) | (b & d) | (c & d) 
                k = 0x8F1BBCDC
            elif 60 <= i <= 79:
                f = b ^ c ^ d
                k = 0xCA62C1D6

            temp =  rotl(a, 5) + f + e + k + w[i] & 0xffffffff
            e,d,c,b,a = d,c,rotl(b,30),a,temp

        h0 = h0 + a & 0xffffffff
        h1 = h1 + b & 0xffffffff
        h2 = h2 + c & 0xffffffff
        h3 = h3 + d & 0xffffffff
        h4 = h4 + e & 0xffffffff

    return ('%08x%08x%08x%08x%08x' % (h0, h1, h2, h3, h4))

"""
def conflit(file1,file2):
    print(sha1(file1),sha1(file2))
    if sha1(file1)==sha1(file2):
        print("Les deux fichiers ont des hash identiques")
        return True,file1,file2
    else:
        print("Les deux fichiers ont des hash différents")
        return False,file1,file2

def tableau(file1,file2):
    L1=sha1(file1)[1]
    L2=sha1(file2)[1]
    for i in range(len(L1)):
        print(i+1,"\t",L1[i],"\t",L2[i])
    print(sha1(file1)[0],sha1(file2)[0])
    for i in range(len(L1)-1):
        if L1[i-1][1]==L1[i+1][6]:
            print("Il y a collision locale à la ligne ",i)
"""

#print(sha1(open('collisions/messageA','rb').read()))
#print(sha1(open('collisions/messageB','r').read()))
print(sha1(open('messages-test/message1.txt','r').read()))
print(sha1("Hello World !"))