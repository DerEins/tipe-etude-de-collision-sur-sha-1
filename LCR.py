#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import outils

def lecture(nomFichier):
    """
    Fonction permettant de lire un fichier .pcap et renvoyant la liste des paquets qu'il contient.

    Synthaxe : lecture(nomFichier)

    Paramètres :
        - nomFichier : Nom du fichier à lire (string).

    Retour : Liste des paquets contenus dans le fichier (liste de bytes).
    """
    with open(nomFichier,'rb') as output:
        octets=output.read()
    octets=octets[40:]
    listePaquets=[]
    temp=b''
    i=8
    while i<len(octets):
        while temp[-8:]!=b'\x00\x00\x38\00\x2f\x40\x40\xa0' and i<len(octets):
            temp+=octets[i].to_bytes(1, byteorder='big')
            i+=1
        listePaquets.append(temp[:-8])
        temp=b''
    return listePaquets

def extraction(listePaquets):
    """
    Fonction permettant d'extraire le vecteur d'initialisation et les premiers octets d'une liste de paquets d'un fichier '.pcap'. 

    Synthaxe : extraction(listePaquets)

    Paramètres :
        - listePaquets : Liste des paquets dont on veut extraire les octets intéressants (liste de bytes).

    Retour : Liste des paquets extraits (liste de bytes).
    """
    ListeIV=[]
    for i in range(len(listePaquets)):
        type=listePaquets[i][56-8].to_bytes(1, byteorder='big')
        if type==b'\x08':
            ListeIV.append(listePaquets[i][80-8:83-8]+listePaquets[i][84-8:84+7-8])
        elif type==b'\x88': 
            ListeIV.append(listePaquets[i][82-8:85-8]+listePaquets[i][86-8:86+7-8])
    return ListeIV

