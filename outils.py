#!/usr/bin/env python3
# -*- coding: utf-8 -*-

def pos(L,el):
    """
    Algorithme permettant de determiner la position d'un élément dans une liste.

    Synthaxe : pos(L,el)

    Paramètres :
        - L = Liste dans laquelle on cherche la position de l'élément souhaité (list).
        - el = élément dont on cherche la position (éléments de L).

    Retour : position de l'élément souhaité dans la liste  (int).
    """
    for i in range(len(L)):
        if L[i]==el:
            return i

def byte_xor(ba1, ba2):
    """
    Algorithme permettant de faire l'opération "Xor" ("Ou exclusif", addition) entre deux éléments binaires bit à bit.

    Synthaxe : byte_xor(byte1,byte2).

    Paramètres :
        - byte1 : première liste d'octets à additionner (bytes).
        - byte2 : seconde liste d'octets à additionner (bytes).

    Retour : le binaire résultant de l'addition (bytes).
    """
    return bytes([_a ^ _b for _a, _b in zip(ba1, ba2)])

def creerFichier(listePaquets):
    """
    Programme permettant de stocker une liste de paquets dans un fichier binaire nommé "N"+nombre de paquets+"f"+longueur d'un paquet.

    Synthaxe : creerFichier(listePaquets)

    Paramètres :
        - listePaquets : liste de paquets à écrire (liste de bytes).

    Retour : Nom du fichier créé (string)
    """
    nomFichier = 'N'+str(len(listePaquets))+'f'+str(len(listePaquets[0]))
    with open(nomFichier, "wb") as filout:
        for paquet in listePaquets:
            filout.write(paquet)
    return nomFichier

def lecturePaquets(nomFichier, lenPaquet):
    """
    Fonction permettant de lire un fichier comportant des paquets WEP et les formater pour l'attaque FMS.

    Synthaxe : lecturePaquets(nomFichier, lenPaquet)

    Paramètres :
        - nomFichier : nom du fichier contenant les paquets à lire (string).
    """
    octets = b''
    with open(nomFichier, "rb") as filout:
        octets = filout.read()
    listePaquet = []
    paquet = b''
    for i in range(0, len(octets), lenPaquet):
        paquet = octets[i:i+lenPaquet]
        listePaquet.append([paquet[:3], paquet[3:]])
    return listePaquet