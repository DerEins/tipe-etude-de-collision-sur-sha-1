# TIPE : Étude de la faillabilité de SHA-1

## Fonctionnement de SHA-1
### Fonction de hashage
Définition : Fonction qui à un message donné associe une empreinte de longueur fixe

Notations :
- $M$ : ensemble des éléments à hasher (souvent infini)
- $E$ : ensemble des empreintes possibles. Pour SHA-1, $\# E = 2^{160}$
- $h$ : fonction de hashage utilisé. Ici, c'est SHA-1

Propriétés attendues :
- Toute modification d'un message doit donner une empreinte radicalement différente. Plus généralement, deux messages différents ne peuvent avoir la même empreinte.

Définitions : 
- Une fonction de hashage est dite parfaite si elle est injective.
- Elle est dite minimale si elle est parfaite et qu'elle est une endofonction (M=E)

Définition : Collision
	On dit qu'il y a collision dans une fonction de hashage lorsque qu'il existe un couple de messages distincts tel que leur hash soit identique
    $\exist (x,y) \in M^2  \ x\neq y \implies h(x)=h(y)$
    
Théorème : $\# E < \# M \implies \exist (x,y) \in M^2 \, x\neq y \wedge f(x) = f(y)$
- Toute fonction de hashage (hormi les endofonction, inutile en pratique) possède au moins une collision.
- On a alors $n_{\text{collisions}} = \# M - \# E$

Conclusion : Un bon algorithme de hashage doit comporter un nombre minimal de collisions.
### Attaque des anniversaires

### Fonctionnement de SHA-1

## Optimisation de l'attaque
### Attaque des anniversaires
Une première amélioration possible d'une attaque par force brute est l'utilisation du paradoxe des anniversaire (à expliquer)

Résultat : La complexité de recherche d'une collision à SHA-1 est réduite à $2^{\frac{160}{2}} = 2^{80}$

## Méthode de l'attaque de SHA-1 par préfixes choisis.
### Attaque par préfixes choisis
L'objectif est, en choisissant des messages $P_1$ et $P_2$ (appelés préfixes), de trouver deux messages différents $M$ et $M'$ tel que $\mathrm{H}(P||M) = \mathrm{H}(P'||M')$. L'intéret est que ce type d'attaque est plus dangereuse, car elle permet à l'attaquant de, par exemple, falsifier une signature électronique en choisissant un en-tête (qui serait alors le préfixe).
### Procédé de l'attaque
Voir la partie 4 de l'article suivant : https://doi.org/10.1007/978-3-030-17659-4_18
1. L'attaquant construit deux graphes $\mathcal{S}$ et $\mathcal{G}$ tel que :
    - $\mathcal{S}$ comporte un ensemble de différences pouvant être annulés avec des blocs à collision proche.
    - $\mathcal{G}$ soit utilisé pour trouver la suite de blocs pour annuler une différence de $\mathcal{S}$.
 2. L'attaquant prépare ses deux préfixes en les mettant à la même longueur. Après traitement, il obtient l'état $cv/cv'$ dont la différence est notée $\delta_R$.
 3. L'attaquant cherche une paire de bloc-message $(u/u')$ dont une paire possède une différence $\delta = H(P_1||u_1)-H(P_1||u'_1)$ appartenant à $S$. Ceci se fait par attaque des anniversaires avec l'algorithme de Van Oorschot and Wiener. La complexité de cette attaque est de l'ordre de $\sqrt{2^n/\#S}$ 

## Réorientation du TIPE.

On étudiera l'algorithme SHA-1 à travers WEP et on étudiera le fait qu'un algorithme diffcilement craquable aujourd'hui l'est beaucoup moins aujourd'hui.

## Fonctionnement du protocole WEP
Voir document FR - Wifi protocole WEP_ mécanismes et failles.pdf + fichier `WEP.py`
## Réutilisation du Keystream
Le fait que le vecteur d'initialisation soit trop court (24 bits soit 17 millions de vecteurs d'initialisation possibles) implique qu'un utilisateur va utiliser plusieurs fois la même keystream.
On peut ainsi faire une analyse passive du réseau pour récupérer les trames de données échangés. Lorsqu'une collision survient, une 

## Attaque par clé apparentée


