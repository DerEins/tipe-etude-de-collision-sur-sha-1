#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import WEP
import random as rd

def generationPaquets(n,lenPaquet,CleWEP):
    """
    Programme permettant de générer des paquets aléatoires chiffrés par WEP de longueur choisie.

    Synthaxe : generateurPaquet(n, lenPaquet, Key))

    Paramètres :
        - n : Nombre de paquets à générer (type int).
        - lenPaquet : Longueur totale des paquet à générer, IV et ICV compris (type int).
        - CleWEP : Clé WEP choisir pour chiffrer les paquets (type byte).

    Retour : Liste des paquets générés (paquets de type byte).
    """
    listePaquets=[]
    for i in range(n):
        M=rd.randbytes(lenPaquet-8)
        listePaquets.append(WEP.Paquet(CleWEP,M))
    return listePaquets

#Exemple de génération de paquets WEP :
CleWEP=b'\x5d\x3a\x7c\x34\x5a'
generationPaquets(6000000,10,CleWEP)